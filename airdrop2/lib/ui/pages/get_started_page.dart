part of 'pages.dart';

class GetStarted extends StatelessWidget {
  const GetStarted({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kBackgroundColor,
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/image_get_started.png'),
                      fit: BoxFit.cover)),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'Fly Like a Bird',
                    style: whiteTextStyle.copyWith(
                        fontSize: 32, fontWeight: semiBold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Explore new world with us and let\nyourself get an amazing experiences',
                    style: whiteTextStyle.copyWith(
                      fontSize: 16,
                      fontWeight: light,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                      height: 55,
                      width: 220,
                      margin: EdgeInsets.only(top: 50, bottom: 80),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: kPrimaryColor,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(defaultRadius))),
                        onPressed: () {
                          Navigator.pushNamed(context, '/sign-up');
                        },
                        child: Text('Get Started',
                            style: whiteTextStyle.copyWith(
                              fontSize: 18,
                            )),
                      ))
                ],
              ),
            )
          ],
        ));
  }
}
