import 'dart:async';
import 'package:airdrop2/shared/shared.dart';
import 'package:flutter/material.dart';

part 'splash_page.dart';
part 'get_started_page.dart';
part 'sign_up_page.dart';
part 'bonus_page.dart';
part 'main_page.dart';
