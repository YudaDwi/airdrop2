part of 'pages.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget customBottomNavigation() {
      return Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: EdgeInsets.only(
                bottom: 30, right: defaultMargin, left: defaultMargin),
            height: 60,
            width: double.infinity,
            decoration: BoxDecoration(
                color: kWhiteColor, borderRadius: BorderRadius.circular(18)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(),
                    Container(
                      height: 24,
                      width: 24,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/icon_home.png'))),
                    ),
                    Container(
                      width: 30,
                      height: 2,
                      decoration: BoxDecoration(
                          color: kPrimaryColor,
                          borderRadius: BorderRadius.circular(18)),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(),
                    Container(
                      height: 24,
                      width: 24,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/icon_booking.png'))),
                    ),
                    Container(
                      width: 30,
                      height: 2,
                      decoration: BoxDecoration(
                          color: kTransparentColor,
                          borderRadius: BorderRadius.circular(18)),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(),
                    Container(
                      height: 24,
                      width: 24,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/icon_card.png'))),
                    ),
                    Container(
                      width: 30,
                      height: 2,
                      decoration: BoxDecoration(
                          color: kTransparentColor,
                          borderRadius: BorderRadius.circular(18)),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(),
                    Container(
                      height: 24,
                      width: 24,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/icon_settings.png'))),
                    ),
                    Container(
                      width: 30,
                      height: 2,
                      decoration: BoxDecoration(
                          color: kTransparentColor,
                          borderRadius: BorderRadius.circular(18)),
                    )
                  ],
                )
              ],
            ),
          ));
    }

    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: Stack(
        children: [customBottomNavigation()],
      ),
    );
  }
}
