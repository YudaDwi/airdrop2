part of 'shared.dart';

double defaultMargin = 24.0;
double defaultRadius = 17.0;

//theme warna
//0x artinya
Color kPrimaryColor = Color(0xff5C40CC);
Color kWhiteColor = Colors.white;
Color kBlackColor = Color(0xff1F1449);
Color kGreyColor = Color(0xff9698A9);
Color kGreenColor = Color(0xff0EC3AE);
Color kRedColor = Color(0xffEB70A5);
Color kBackgroundColor = Color(0xffDBD7EC);
Color kTransparentColor = Colors.transparent;
//theme textstyle
TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlackColor);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhiteColor);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGreyColor);
TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPrimaryColor);
TextStyle greenTextStyle = GoogleFonts.poppins(color: kGreenColor);
TextStyle redTextStyle = GoogleFonts.poppins(color: kRedColor);

//theme weight
FontWeight light = FontWeight.w300;
FontWeight reguler = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;
